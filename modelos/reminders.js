var mongoose = require('mongoose')

var schema = new mongoose.Schema({
  email: String,
  anniversary: Date,
  activities: Array,
  birthDate: Date,
  rememberBirthdate: Boolean,
  sex: String
})
var reminder = mongoose.model('reminder', schema)

module.exports = reminder