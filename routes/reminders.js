var express = require('express');
var router = express.Router();
var validate = require('express-validation')
var Joi = require('joi')
const Reminders = require('../modelos/reminders')
const {
  asyncHelper
} = require('../utils')

router.post('/', validate({
    body: {
      email: Joi.string().email().required(),
      anniversary: Joi.string().isoDate().required()
    }
  })

  , asyncHelper(async (req, res, next) => {

    const reminder = await Reminders.create({
      email: req.body.email,
      anniversary: req.body.anniversary
    })
    return res.send(reminder)
  }))

router.patch('/:id', validate({
    params: {
      id: Joi.string().required()
    },
    body: {
      activities: Joi.array().required(),
      birthDate: Joi.string().isoDate().required(),
      rememberBirthdate: Joi.boolean().required(),
      sex: Joi.string().required()
    }
  }),
  asyncHelper(async (req, res, next) => {

    const reminder = await Reminders.findByIdAndUpdate(req.params.id, req.body, {
      new: true
    })

    return res.send(reminder.toObject())
  })

)



module.exports = router