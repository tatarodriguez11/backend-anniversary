var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose')
var debug = require('debug')('backend:app.js')

mongoose.connect('mongodb://tata:123456Ab@ds147995.mlab.com:47995/maravillarte', {
  useNewUrlParser: true
})


mongoose.connection.on('connected', () => debug('connected :)'))
mongoose.connection.on('error', () => debug('error'))
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var remindersRouter = require('./routes/reminders')



var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/reminders', remindersRouter)

module.exports = app;