const test = require('ava')
const request = require('supertest')
const app = require('../app')
const Reminders = require('../modelos/reminders')

test('POST /reminders, Bad Request', async (t) => {
  const response = await request(app).post('/reminders').send({})
  t.is(response.status, 400)
})

test('POST /reminders, OK', async (t) => {
  const response = await request(app).post('/reminders').send({
    email: "ejemplo@mail.com",
    anniversary: "2019-05-21T05:00:00.000Z"
  })
  t.is(response.status, 200)
  t.truthy(response.body._id)
  const reminder = await Reminders.findById(response.body._id)
  t.truthy(reminder)
})

test('PATCH /reminders/:id, Bad Request', async (t) => {
  const response = await request(app).patch('/reminders/:id').send({})
  t.is(response.status, 400)
})

test('PATCH /reminders/:id, OK', async (t) => {
  const reminder = await Reminders.create({
    email: "ejemplo@mail.com",
    anniversary: "2019-05-21T05:00:00.000Z"
  })
  const response = await request(app).patch(`/reminders/${reminder._id}`).send({
    activities: [
      "DANCE",
      "EXTREME_SPORTS"
    ],
    "birthDate": "1962-09-04T05:00:00.000Z",
    "rememberBirthdate": true,
    "sex": "MALE"
  })
  t.is(response.status, 200)
  const updatedReminder = (await Reminders.findById(reminder._id)).toObject()
  t.is(updatedReminder.sex, 'MALE')
  t.deepEqual(updatedReminder.activities, [
    "DANCE",
    "EXTREME_SPORTS"
  ])
  t.truthy(updatedReminder.birthDate)
  t.is(updatedReminder.rememberBirthdate, true)
})